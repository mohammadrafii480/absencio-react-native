const initialState = {
  bahasa: 'id',
  register: {},
  base64: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'CHANGE_LANGUAGE':
      return {
        ...state,
        bahasa: action.payload,
      };
    case 'ACTIVATE_ACCOUNT':
      return {
        ...state,
        register: action.payload,
      };
    case 'SET_BASE64':
      return {
        ...state,
        base64: action.payload,
      };
    default:
      return state;
  }
};

export default reducer;
