// Tindakan untuk menambahkan tugas ke daftar
export const aktifkanAkun = payload => {
  return {
    type: 'ACTIVATE_ACCOUNT',
    payload: payload,
  };
};
export const changeLanguage = language => {
  return {
    type: 'CHANGE_LANGUAGE',
    payload: language,
  };
};
export const setBase64 = base64 => {
  return {
    type: 'SET_BASE64',
    payload: base64,
  };
};
