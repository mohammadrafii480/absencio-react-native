import axiosInstance from '../Helper/axiosInstance';
export const newTask = async payload => {
  return await axiosInstance.post('todo/new_task', payload);
};
export const updateTask = async payload => {
  return await axiosInstance.post('todo/new_task', payload);
};
export const update_status_task = async payload => {
  return await axiosInstance.post('todo/update_status', payload);
};
export const delete_task = async payload => {
  return await axiosInstance.post('todo/delete_task', payload);
};
export const task_list = async payload => {
  return await axiosInstance.post('todo/task_list', payload);
};
export const spvUpdatePengajuan = async payload => {
  return await axiosInstance.post('supervisi/pengajuan_update', payload);
};
export const spvUpdatePengajuanHybrid = async payload => {
  return await axiosInstance.post('supervisi/pengajuan_update_hybrid', payload);
};
export const spvGetData = async payload => {
  return await axiosInstance.post('supervisi/get_data', payload);
};
export const spvGetKaryawanList = async payload => {
  return await axiosInstance.post('supervisi/karyawan_list', payload);
};
export const spvGetPengajuanList = async payload => {
  return await axiosInstance.post('supervisi/pengajuan_list', payload);
};
export const spvGetPengajuanHybridList = async payload => {
  return await axiosInstance.post(
    'supervisi/pengajuan_hybrid_list_spv',
    payload,
  );
};
export const spvGetKunjunganList = async payload => {
  return await axiosInstance.post('supervisi/kunjungan_list', payload);
};
export const verifyFR = async payload => {
  return await axiosInstance.post('verify', payload);
};
export const Register = async payload => {
  return await axiosInstance.post('auth/register', payload);
};
export const Login = async payload => {
  return await axiosInstance.post('auth/login', payload);
};
export const pengajuanHybrid = async payload => {
  return await axiosInstance.post('kehadiran/pengajuan_hybrid', payload);
};
export const Logout = async payload => {
  return await axiosInstance.post('auth/log_out', payload);
};
export const sendCode = async payload => {
  return await axiosInstance.post('auth/send_email', payload);
};
export const sendOtp = async payload => {
  return await axiosInstance.post('auth/sms_otp', payload);
};
export const verifyCode = async payload => {
  return await axiosInstance.post('auth/code_verify', payload);
};
export const verifyOtp = async payload => {
  return await axiosInstance.post('auth/otp_verify', payload);
};
export const sendLocBackground = async payload => {
  return await axiosInstance.post('karyawan/send_location', payload);
};
export const vermuk = async payload => {
  return await axiosInstance.post('karyawan/vermuk', payload);
};
export const changeProfile = async payload => {
  return await axiosInstance.post('karyawan/ubah_profile', payload);
};
export const assignmentToday = async payload => {
  return await axiosInstance.post('assignment/assignment_today', payload);
};
export const kehadiranMonth = async payload => {
  return await axiosInstance.post('kehadiran/history_kehadiran_month', payload);
};
export const deletePengajuanHybrid = async payload => {
  return await axiosInstance.post('kehadiran/delete_hybrid', payload);
};
export const kehadiranList = async payload => {
  return await axiosInstance.post('kehadiran/kehadiran_list', payload);
};
export const assignmentList = async payload => {
  return await axiosInstance.post('assignment/tugas_list', payload);
};
export const riwayatAssignmentList = async payload => {
  return await axiosInstance.post('assignment/riwayat_tugas_list', payload);
};
export const cekRadius = async payload => {
  return await axiosInstance.post('kehadiran/cek_jarak', payload);
};
export const cekRadiusKunjungan = async payload => {
  return await axiosInstance.post('kunjungan/cek_jarak', payload);
};
export const checkOutKunjungan = async payload => {
  return await axiosInstance.post('karyawan/kunjungan', payload);
};
export const checkIn = async payload => {
  return await axiosInstance.post('kehadiran/check_in', payload);
};
export const checkOut = async payload => {
  return await axiosInstance.post('kehadiran/check_out', payload);
};
export const cuti = async payload => {
  return await axiosInstance.post('karyawan/cuti', payload);
};
export const deleteCuti = async payload => {
  return await axiosInstance.post('cuti/delete_cuti', payload);
};
export const tugasKerja = async payload => {
  return await axiosInstance.post('supervisi/buat_tugas', payload);
};
export const tugasKerjaList = async payload => {
  return await axiosInstance.post('supervisi/tugas_spv_list', payload);
};
export const ubahKunjungan = async payload => {
  return await axiosInstance.post('kunjungan/ubah_kunjungan', payload);
};
export const updateTugas = async payload => {
  return await axiosInstance.post('assignment/update_assignment', payload);
};
export const cutiList = async payload => {
  return await axiosInstance.post('cuti/cuti_list', payload);
};
export const pengajuanHybridList = async payload => {
  return await axiosInstance.post('kehadiran/pengajuan_hybrid_list', payload);
};
export const kunjunganList = async payload => {
  return await axiosInstance.post('kunjungan/kunjungan_list', payload);
};
export const ubahFoto = async payload => {
  return await axiosInstance.post('karyawan/ubah_foto', payload);
};
export const getData = async payload => {
  return await axiosInstance.post('karyawan/get_data', payload);
};
export const cekPerusahaan = async payload => {
  return await axiosInstance.post('karyawan/cek_perusahaan', payload);
};
