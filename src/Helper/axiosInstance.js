import axios from 'axios';
import {Host} from '../Api/Host';
const axiosInstance = axios.create({
  baseURL: Host.mainHost,
  headers: {
    // Authorization: 'Bearer your_token_here',
    'Content-Type': 'application/json',
  },
});
export default axiosInstance;
