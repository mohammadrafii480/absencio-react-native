import {Platform, ToastAndroid, Alert} from 'react-native';
const showToast = message => {
  if (Platform.OS === 'ios') {
    Alert.alert(message);
  } else {
    ToastAndroid.show(message, ToastAndroid.SHORT);
  }
};
export default showToast;
