import React, {useState, useEffect, useRef, useMemo, useCallback} from 'react';
import {
  absensio_logo,
  background,
  flappybird,
  v_dialog_success,
  v_dialog_wrong,
} from '../Asset';
import {request, PERMISSIONS, RESULTS} from 'react-native-permissions';
import {
  Image,
  ImageBackground,
  StyleSheet,
  TextInput,
  View,
  Text,
  Modal,
  Pressable,
  TouchableOpacity,
  Keyboard,
} from 'react-native';
import messaging from '@react-native-firebase/messaging';
import {useSelector} from 'react-redux';
import showToast from '../Helper/showAlert';
import {Button} from 'react-native-paper';
import Color from '../Color';
import {id, en} from '../String/text';
import {BottomSheetModal} from '@gorhom/bottom-sheet';
import {BottomSheetModalProvider} from '@gorhom/bottom-sheet';
import {sendCode, verifyCode} from '../Api/Endpoint';
const Login = ({navigation}) => {
  const [fields, setField] = useState({});
  const [showPopUpFailed, setShowPopUpFailed] = useState(false);
  const [showPopUpSuccess, setShowPopUpSuccess] = useState(false);
  const bottomSheetModalRef = useRef(null);
  const snapPoints = useMemo(() => ['25%', '70%'], []);
  const onChangeInputValue = fieldName => value => {
    setField({
      ...fields,
      [fieldName]: value,
    });
  };
  const onPressButton = useCallback(() => {
    if (fields.hasOwnProperty('email') && fields?.email.length > 0) {
      Keyboard.dismiss();
      console.log('hasil', fields);
      sendCode({email: fields?.email})
        .then(response => {
          console.log('Response', response.data);
          if (response.data.err_code === 0) {
            bottomSheetModalRef.current?.present();
          } else {
            showToast(response.data.message);
          }
        })
        .catch(err => {
          showToast(
            bahasa === 'id' ? id.err + ' - ' + err : en.err + ' - ' + err,
          );
        });
    } else {
      showToast(
        bahasa === 'id'
          ? id.silahkan_isi_email_anda
          : en.silahkan_isi_email_anda,
      );
    }
  });
  const onVerifyButton = useCallback(() => {
    if (fields.hasOwnProperty('otp') && fields?.otp.length > 0) {
      Keyboard.dismiss();
      console.log('hasil', fields);
      verifyCode({code: fields?.otp, email: fields?.email})
        .then(response => {
          console.log('Response', response.data);
          if (response.data.err_code === 0) {
            var signature = response.data.signature;
            setShowPopUpSuccess(true);
            setTimeout(() => {
              setShowPopUpSuccess(false);
              getTokenFirebase(signature);
              bottomSheetModalRef.current?.close();
            }, 2000);
          } else {
            setShowPopUpFailed(true);
          }
        })
        .catch(err => {
          showToast(
            bahasa === 'id' ? id.err + ' - ' + err : en.err + ' - ' + err,
          );
        });
    } else {
      showToast(
        bahasa === 'id' ? id.silahkan_isi_kode_anda : en.silahkan_isi_kode_anda,
      );
    }
  });
  const handleSheetChanges = useCallback(index => {
    console.log('handleSheetChanges', index);
  }, []);
  const requestCameraPermission = async () => {
    try {
      const result = await request(
        Platform.OS === 'ios'
          ? PERMISSIONS.IOS.CAMERA
          : PERMISSIONS.ANDROID.CAMERA,
      );
      if (result === RESULTS.GRANTED) {
        console.log('Camera permission granted.');
      } else if (result === RESULTS.DENIED) {
        showToast('Camera permission denied.');
      } else if (result === RESULTS.BLOCKED) {
        showToast('Camera permission blocked.');
      }
    } catch (error) {
      console.error(error);
    }
  };
  const requestLocationPermission = async () => {
    try {
      const result = await request(
        Platform.OS === 'ios'
          ? PERMISSIONS.IOS.LOCATION_ALWAYS
          : PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
      );
      if (result === RESULTS.GRANTED) {
        console.log('Location permission granted.');
      } else if (result === RESULTS.DENIED) {
        showToast('Location permission denied.');
      } else if (result === RESULTS.BLOCKED) {
        showToast('Location permission blocked.');
      }
    } catch (error) {
      console.error(error);
    }
  };
  const requestStoragePermission = async () => {
    try {
      const result = await request(
        Platform.OS === 'ios'
          ? PERMISSIONS.IOS.MEDIA_LIBRARY
          : PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,
      );
      if (result === RESULTS.GRANTED) {
        console.log('Storage permission granted.');
      } else if (result === RESULTS.DENIED) {
        showToast('Storage permission denied.');
      } else if (result === RESULTS.BLOCKED) {
        showToast('Storage permission blocked.');
      }
    } catch (error) {
      console.error(error);
    }
  };
  const popUpSuccess = () => {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={showPopUpSuccess}
        statusBarTranslucent
        onRequestClose={() => {
          setShowPopUpSuccess(false);
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <Image
                source={v_dialog_success}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id' ? id.true_otp : en.true_otp}
              </Text>
            </View>
          </View>
        </View>
      </Modal>
    );
  };
  const popUpFailed = () => {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={showPopUpFailed}
        statusBarTranslucent
        onRequestClose={() => {
          setShowPopUpFailed(false);
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <Image
                source={v_dialog_wrong}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id' ? id.wrong_otp : en.wrong_otp}
              </Text>
              <Pressable
                style={{
                  width: '100%',
                  height: 43,
                  borderRadius: 10,
                  padding: 10,
                  elevation: 2,
                  backgroundColor: Color.red,
                }}
                onPress={() => {
                  setShowPopUpFailed(false);
                }}>
                <Text
                  style={{
                    alignSelf: 'center',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    fontFamily: 'Helvetica',
                    fontSize: 17,
                    color: 'white',
                  }}>
                  {bahasa === 'id' ? id.kembali : en.kembali}
                </Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
    );
  };
  const getTokenFirebase = async signature => {
    console.log('Signature', signature);
    try {
      const token = await messaging().getToken();
      console.log('Firebase Messaging Token:', token);
    } catch (error) {
      console.log('Error getting Firebase Messaging Token:', error);
    }
  };
  const handleTextInputSubmit = () => {
    // Menutup keyboard saat teks input dikirimkan (misalnya, dengan menekan tombol "Enter" di keyboard)
    Keyboard.dismiss();
  };
  useEffect(() => {
    requestCameraPermission();
    requestLocationPermission();
    requestStoragePermission();
  }, []);
  const bahasa = useSelector(state => state.bahasa);
  return (
    <BottomSheetModalProvider>
      <ImageBackground source={background} style={styles.img_backgroud}>
        <Image
          resizeMode="contain"
          source={absensio_logo}
          style={styles.logo}
        />
        {popUpFailed()}
        {popUpSuccess()}
        <TextInput
          style={styles.textInputEmail}
          onChangeText={onChangeInputValue('email')}
          keyboardType="email-address"
          placeholder={bahasa === 'id' ? id.email : en.email}
        />

        <Button
          onPress={onPressButton}
          accessibilityLabel="submit-button"
          labelStyle={styles.btnLabel}
          style={styles.buttonStyle}
          mode="contained">
          {bahasa === 'id' ? id.masuk : en.masuk}
        </Button>
        <BottomSheetModal
          backgroundStyle={{
            elevation: 500, // Nilai ini mengontrol efek elevation (bayangan) pada card
            shadowColor: 'black',
            shadowOffset: {width: 200, height: 90},
            shadowOpacity: 30,
            shadowRadius: 120,
          }}
          ref={bottomSheetModalRef}
          style={{
            shadowColor: '#000',
            shadowOffset: {width: 0, height: 2},
            shadowOpacity: 3,
            shadowRadius: 5,
            elevation: 5,
          }}
          index={1}
          keyboardBehavior="fillParent"
          enableOverDrag={false}
          onChange={handleSheetChanges}
          snapPoints={snapPoints}>
          <View
            style={{
              alignItems: 'center',
              width: '100%',
              height: '100%',
              flexDirection: 'column',
            }}>
            <Image
              resizeMode="contain"
              source={flappybird}
              style={{marginTop: '5%'}}
            />
            <Text style={styles.text_akun_anda_belum_aktif('normal')}>
              {bahasa === 'id'
                ? id.redaksi_verify_email
                : en.redaksi_verify_email}
            </Text>
            <TextInput
              style={styles.textInputOtp}
              onChangeText={onChangeInputValue('otp')}
              onSubmitEditing={handleTextInputSubmit}
              keyboardType="numeric"
              placeholder={
                bahasa === 'id' ? id.kode_autentikasi : en.kode_autentikasi
              }
            />
            <Button
              onPress={onVerifyButton}
              accessibilityLabel="submit-button"
              labelStyle={styles.btnLabel}
              style={styles.buttonStyle}
              mode="contained">
              {bahasa === 'id' ? id.verifikasi : en.verifikasi}
            </Button>
            <TouchableOpacity
              onPress={() => {
                // sendCode({email: fields?.email})
                //   .then(response => {
                //     console.log('Response', response.data);
                //   })
                //   .catch(err => {
                //     showToast(bahasa === 'id' ? id.err : en.err);
                //   });
              }}>
              <Text style={styles.text_akun_anda_belum_aktif('normal')}>
                {bahasa === 'id' ? id.kirim_ulang_kode : en.kirim_ulang_kode}
              </Text>
            </TouchableOpacity>
          </View>
        </BottomSheetModal>
        <View style={styles.view_aktifkan_akun}>
          <Text style={styles.text_akun_anda_belum_aktif('normal')}>
            {bahasa === 'id' ? id.akunAndaBelumAktif : en.akunAndaBelumAktif}
          </Text>
          <TouchableOpacity
            onPress={() => {
              navigation.replace('Register');
            }}>
            <Text style={styles.text_akun_anda_belum_aktif('bold')}>
              {bahasa === 'id' ? id.aktifkanDisini : en.aktifkanDisini}
            </Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </BottomSheetModalProvider>
  );
};
export default Login;
const styles = StyleSheet.create({
  text_akun_anda_belum_aktif: style => ({
    fontFamily: 'Helvetica',
    fontSize: 14,
    color: 'black',
    fontWeight: style,
  }),
  view_aktifkan_akun: {
    flexDirection: 'row',
    width: '100%',
    height: 'auto',
    marginBottom: '8%',
    justifyContent: 'center',
  },
  logo: {
    width: '100%',
    height: '16%',
  },
  img_backgroud: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'flex-end',
  },
  btnLabel: {
    fontFamily: 'Helvetica',
    fontWeight: 'bold',
    fontSize: 15,
  },
  textInputOtp: {
    borderColor: Color.borderTextInput,
    borderWidth: 1.5,
    borderRadius: 6,
    backgroundColor: 'white',
    height: 50,
    width: '90%',
    marginBottom: '5%',
    marginTop: '5%',
    paddingStart: 10,
  },
  textInputEmail: {
    borderColor: Color.borderTextInput,
    borderWidth: 1.5,
    borderRadius: 6,
    backgroundColor: 'white',
    height: 50,
    marginStart: '5%',
    marginEnd: '5%',
    marginBottom: '8%',
    marginTop: '20%',
    paddingStart: 10,
  },
  buttonStyle: {
    borderRadius: 6,
    backgroundColor: Color.main,
    marginBottom: '8%',
    width: '90%',
    height: 45,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
