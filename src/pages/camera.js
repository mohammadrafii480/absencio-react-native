import React, {useState, useRef} from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  StyleSheet,
  ImageBackground,
  LogBox,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import {id, en} from '../String/text';
import {back, bgfoto} from '../Asset';
import {Text} from 'react-native-paper';
import {useSelector, useDispatch} from 'react-redux';
import RNFS from 'react-native-fs';
import ImageEditor from '@react-native-community/image-editor';
import {setBase64} from '../redux/action';
LogBox.ignoreAllLogs();
const Camera = ({navigation}) => {
  const dispatch = useDispatch();
  const [canDetectFaces, setCanDetectFaces] = useState(true);
  const [doneCapture, setDoneCapture] = useState(false);
  const [blinkDetected, setBlinkDetected] = useState(false);
  const camera = useRef(null);
  const takePicture = async () => {
    console.log('<=== AMBIL GAMBAR ===>');
    if (camera.current) {
      setDoneCapture(true);
      const options = {quality: 0.5, base64: true};
      const data = await camera.current.takePictureAsync(options);

      const cropData = {
        offset: {
          x: 0,
          y: 200,
        },
        size: {width: 3000, height: 3000},
        displaySize: {width: 480, height: 900},
        resizeMode: 'contain',
      };
      //Crop Image PEOPLE
      ImageEditor.cropImage(data.uri, cropData).then(url => {
        //Convert Uri to Base64
        RNFS.readFile(url, 'base64').then(res => {
          var uri = url;
          var base64 = res;
          dispatch(setBase64(base64));
          navigation.replace('Register');
          //console.log('base64', base64);
        });
      });
    }
  };
  const facesDetectedselfie = faces => {
    if (faces.faces.length > 0) {
      console.log('faces', faces.faces);
      const rightEye = faces.faces[0].rightEyeOpenProbability;
      const leftEye = faces.faces[0].leftEyeOpenProbability;
      const bothEyes = (rightEye + leftEye) / 2;
      !doneCapture
        ? console.log(
            'rightEye = ' +
              rightEye +
              ' | leftEye = ' +
              leftEye +
              ' | bothEyes = ' +
              bothEyes,
          )
        : null;
      if (!doneCapture && bothEyes < 0.3) {
        setBlinkDetected(true);
        setCanDetectFaces(false);
        setTimeout(() => {
          takePicture();
        }, 500);
      }
      if (blinkDetected && bothEyes >= 0.9) {
        setBlinkDetected(false);
      }
    }
  };
  const bahasa = useSelector(state => state.bahasa);
  return (
    <RNCamera
      captureAudio={false}
      ref={camera}
      style={styles.camStyle}
      onCameraReady={() => {
        setCanDetectFaces(true);
        console.log('Face detected');
      }}
      faceDetectionMode={RNCamera.Constants.FaceDetection.Mode.accurate}
      faceDetectionLandmarks={RNCamera.Constants.FaceDetection.Landmarks.all}
      faceDetectionClassifications={
        RNCamera.Constants.FaceDetection.Classifications.all
      }
      onFacesDetected={faces => {
        !doneCapture ? console.log('Face detected', canDetectFaces) : null;
        if (faces.faces.length === 0) {
          setCanDetectFaces(false);
        } else {
          setCanDetectFaces(true);
          facesDetectedselfie(faces);
        }
      }}
      onFaceDetectionError={error => console.log('FDError', error)} // This is never triggered
      type={'front'}>
      <View style={styles.viewUpper}>
        <ImageBackground source={bgfoto} style={styles.bgIntruksi}>
          <Text style={styles.teksIntruksi}>
            {bahasa === 'id'
              ? id.intruksi_kedipkan_mata
              : en.intruksi_kedipkan_mata}
          </Text>
        </ImageBackground>
      </View>
      <View style={styles.viewBottom}>
        <TouchableOpacity
          onPress={() => {
            navigation.replace('Register');
          }}>
          <Image resizeMode="center" source={back} style={styles.img} />
        </TouchableOpacity>
        <Text style={styles.textKembali}>
          {bahasa === 'id' ? id.kembali : id.kembali}
        </Text>
      </View>
    </RNCamera>
  );
};
export default Camera;
const styles = StyleSheet.create({
  teksIntruksi: {
    textAlign: 'center',
    width: '60%',
    alignSelf: 'center',
    fontFamily: 'Helvetica',
    fontSize: 18,
    color: 'black',
  },
  bgIntruksi: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
  },
  viewUpper: {
    backgroundColor: 'white',
    height: 70,
    margin: 30,
    flexDirection: 'row',
  },
  viewBottom: {
    height: 50,
    marginBottom: 20,
    width: '100%',
    alignItems: 'center',
    flexDirection: 'row',
  },
  camStyle: {
    justifyContent: 'space-between',
    alignSelf: 'center',
    alignItems: 'center',
    height: '100%',
    flexDirection: 'column',
    width: '100%',
  },
  img: {
    width: 20,
    height: 20,
    marginStart: 20,
    marginEnd: 10,
    tintColor: 'white',
  },
  textKembali: {
    fontFamily: 'Helvetica',
    fontSize: 15,
    color: 'white',
    fontWeight: 'bold',
  },
});
