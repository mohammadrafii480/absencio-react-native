import React, {useEffect} from 'react';
import {absensio_logo, background} from '../Asset';
import {Image, ImageBackground, StyleSheet} from 'react-native';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Login');
    }, 3000);
  }, [navigation]);

  return (
    <ImageBackground source={background} style={styles.img_backgroud}>
      <Image resizeMode="contain" source={absensio_logo} style={styles.logo} />
    </ImageBackground>
  );
};
export default Splash;
const styles = StyleSheet.create({
  logo: {
    width: '100%',
    height: '14%',
    marginBottom: '15%',
  },
  img_backgroud: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'flex-end',
  },
});
