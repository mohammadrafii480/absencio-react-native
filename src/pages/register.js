import React from 'react';
import {
  Image,
  ImageBackground,
  StyleSheet,
  TextInput,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {background, back, image_placeholder, kamera} from '../Asset';
import showToast from '../Helper/showAlert';
import {request, PERMISSIONS, RESULTS} from 'react-native-permissions';
import {Button} from 'react-native-paper';
import Color from '../Color';
import {id, en} from '../String/text';
import {aktifkanAkun, setBase64} from '../redux/action';
const Register = ({navigation}) => {
  const bahasa = useSelector(state => state.bahasa);
  const base64Image = useSelector(state => state.base64);
  var linkURI = `data:image/png;base64,${base64Image}`;
  let encoded = encodeURI(linkURI);
  const payloadRegister = useSelector(state => state.register);
  const dispatch = useDispatch();
  const onChangeInputValue = fieldName => value => {
    dispatch(
      aktifkanAkun({
        ...payloadRegister,
        [fieldName]: value,
      }),
    );
  };
  const requestCameraPermission = async () => {
    try {
      const result = await request(
        Platform.OS === 'ios'
          ? PERMISSIONS.IOS.CAMERA
          : PERMISSIONS.ANDROID.CAMERA,
      );
      if (result === RESULTS.GRANTED) {
        navigation.replace('Camera');
        console.log('Camera permission granted.');
      } else if (result === RESULTS.DENIED) {
        showToast('Camera permission denied.');
        requestCameraPermission();
      } else if (result === RESULTS.BLOCKED) {
        showToast('Camera permission blocked.');
      }
    } catch (error) {
      console.error(error);
    }
  };

  const onPressButton = () => {};
  return (
    <ImageBackground source={background} style={styles.img_backgroud}>
      <View style={styles.topBar}>
        <TouchableOpacity
          onPress={() => {
            navigation.replace('Login');
            dispatch(aktifkanAkun({}));
            dispatch(setBase64(''));
          }}>
          <Image resizeMode="center" source={back} style={styles.imgBack} />
        </TouchableOpacity>
      </View>
      <ScrollView>
        <View style={styles.circle}>
          <Image
            source={base64Image === '' ? image_placeholder : {uri: encoded}}
            style={styles.image_placeholder}
          />
          <TouchableOpacity
            onPress={() => {
              requestCameraPermission();
            }}>
            <Image source={kamera} style={styles.iconCamera} />
          </TouchableOpacity>
        </View>
        <TextInput
          style={styles.textInput(false, 1)}
          onChangeText={onChangeInputValue('noHp')}
          keyboardType="phone-pad"
          defaultValue={payloadRegister?.noHp}
          placeholder={bahasa === 'id' ? id.noHp : en.noHp}
        />
        <TextInput
          style={styles.textInput(false, 1)}
          onChangeText={onChangeInputValue('nama')}
          keyboardType="default"
          defaultValue={payloadRegister?.nama}
          placeholder={bahasa === 'id' ? id.namaLengkap : en.namaLengkap}
        />
        <TextInput
          style={styles.textInput(false, 1)}
          onChangeText={onChangeInputValue('email')}
          keyboardType="email-address"
          defaultValue={payloadRegister?.email}
          placeholder={bahasa === 'id' ? id.email : en.email}
        />
        <TextInput
          style={styles.textInput(false, 1)}
          onChangeText={onChangeInputValue('nik')}
          keyboardType="numeric"
          defaultValue={payloadRegister?.nik}
          placeholder={bahasa === 'id' ? id.NIK : en.NIK}
        />
        <TextInput
          style={styles.textInput(false, 1)}
          onChangeText={onChangeInputValue('nip')}
          keyboardType="numeric"
          defaultValue={payloadRegister?.nip}
          placeholder={bahasa === 'id' ? id.NIP : en.NIP}
        />
        <TextInput
          style={styles.textInput(true, 2)}
          multiline
          textAlignVertical="top"
          numberOfLines={2}
          onChangeText={onChangeInputValue('alamat')}
          keyboardType="default"
          defaultValue={payloadRegister?.alamat}
          placeholder={
            bahasa === 'id' ? id.alamatTempatTinggal : en.alamatTempatTinggal
          }
        />
        <Button
          onPress={onPressButton}
          accessibilityLabel="submit-button"
          labelStyle={styles.btnLabel}
          style={styles.buttonStyle}
          mode="contained">
          {bahasa === 'id' ? id.aktifkan : en.aktifkan}
        </Button>
        <View style={styles.view_aktifkan_akun}>
          <Text style={styles.text_akun_anda_belum_aktif('normal')}>
            {bahasa === 'id' ? id.akunAndaSudahAktif : en.akunAndaSudahAktif}
          </Text>
          <Text
            onPress={() => {
              navigation.replace('Login');
              dispatch(aktifkanAkun({}));
            }}
            style={styles.text_akun_anda_belum_aktif('bold')}>
            {bahasa === 'id' ? id.loginDisini : en.loginDisini}
          </Text>
        </View>
      </ScrollView>
    </ImageBackground>
  );
};
export default Register;
const styles = StyleSheet.create({
  iconCamera: {
    position: 'absolute',
    width: 30,
    height: 30,
    bottom: 0,
    alignSelf: 'center',
    tintColor: 'white',
    marginBottom: 10,
  },
  imgBack: {
    width: 20,
    height: 20,
    marginStart: '2%',
  },
  circle: {
    width: 150,
    height: 150,
    marginTop: '8%',
    marginBottom: '8%',
    borderRadius: 100,
    borderWidth: 5,
    borderColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    elevation: 5, // Nilai ini mengontrol efek elevation (bayangan) pada card
    shadowColor: 'black',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.3,
    shadowRadius: 2,
  },
  image_placeholder: {
    width: 140,
    height: 140,
    borderRadius: 75,
  },
  topBar: {
    backgroundColor: 'white',
    height: 50,
    width: '100%',
    justifyContent: 'center',
    elevation: 5, // Nilai ini mengontrol efek elevation (bayangan) pada card
    shadowColor: 'black',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.3,
    shadowRadius: 2,
  },
  text_akun_anda_belum_aktif: style => ({
    fontFamily: 'Helvetica',
    fontSize: 14,
    color: 'black',
    fontWeight: style,
  }),
  view_aktifkan_akun: {
    flexDirection: 'row',
    width: '100%',
    height: 'auto',
    marginBottom: '8%',
    justifyContent: 'center',
  },
  img_backgroud: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'flex-start',
  },
  btnLabel: {
    fontFamily: 'Helvetica',
    fontWeight: 'bold',
    fontSize: 15,
  },
  textInput: (multiline, numberOfLines) => ({
    borderColor: Color.borderTextInput,
    borderWidth: 1.5,
    borderRadius: 6,
    backgroundColor: 'white',
    width: '90%',
    height: !multiline ? 50 : 45 * numberOfLines,
    marginBottom: '5%',
    paddingStart: 10,
    alignSelf: 'center',
  }),
  buttonStyle: {
    borderRadius: 6,
    backgroundColor: Color.main,
    marginBottom: '8%',
    width: '90%',
    height: 45,
    alignSelf: 'center',
  },
});
