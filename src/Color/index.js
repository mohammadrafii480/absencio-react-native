const Color = {
  main: '#476072',
  borderTextInput: '#DADADA',
  red: '#E34243',
  green: '#3ACE84',
};
export default Color;
