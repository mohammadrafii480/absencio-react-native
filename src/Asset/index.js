import absensio_logo from './absensio_logo.png';
import background from './background.png';
import banner_daftar from './banner_daftar.gif';
import bgfake from './bgfake.png';
import bgfoto from './bgfoto.png';
import back from './back.png';
import image_placeholder from './image_placeholder.png';
import kamera from './kamera.png';
import otp from './otp.gif';
import flappybird from './flappybird.gif';
import v_dialog_info from './v_dialog_info.png';
import v_dialog_success from './v_dialog_success.png';
import v_dialog_warning from './v_dialog_warning.png';
import v_dialog_wrong from './v_dialog_wrong.png';
export {
  image_placeholder,
  absensio_logo,
  background,
  banner_daftar,
  bgfake,
  bgfoto,
  back,
  kamera,
  otp,
  flappybird,
  v_dialog_info,
  v_dialog_success,
  v_dialog_warning,
  v_dialog_wrong,
};
